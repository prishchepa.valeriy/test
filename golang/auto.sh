#!/bin/bash
docker build -t golang-test:latest .
docker stop test-cont
docker rm test-cont
docker run -d --name test-cont golang-test $1
sleep 3
docker ps
docker attach test-cont
