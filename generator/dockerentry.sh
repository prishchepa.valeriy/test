#!/bin/bash
useradd -p $(openssl passwd -1 "$variable2") "$variable1"
sed -i -e 's/UsePrivilegeSeparation yes/UsePrivilegeSeparation no/g' /etc/ssh/sshd_config
/usr/sbin/sshd -D
