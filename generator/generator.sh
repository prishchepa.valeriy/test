#!/bin/bash
variable1=$1
variable2=$2
variable3=$3
docker build -t ubuntu-ssh-server .
docker stop $variable3
docker rm $variable3
docker run --name $variable3 --env variable1=$variable1 --env variable2=$variable2 -d -p $variable3:22 ubuntu-ssh-server
docker ps
